// On include les packages nodeJS nécessaire !
const mongoose    = require('mongoose');
const async       = require('async');
const color       = require('chalk');
const crypto      = require('crypto');

//
// Fonction de cryptage !
//
var sha1sum = ( input ) => {
    return crypto.createHash('sha1').update(JSON.stringify(input)).digest('hex');
}

//
// On récupère le répertoire de notre script!
//
const appDir = process.cwd();

// Création des meta-models MongoDB
const UserDB    = require( appDir + '/data/user.js');
const UserStore = require( appDir + '/data/users.json');

//
// On remove tout les utilisateurs de la database!
//
const remove_Allusers = () => {
    return new Promise( (resolve,reject) => {
        UserDB.remove({},(err) => {
            if(err) {
                reject(err);
            }
            else {
                console.info(color.yellow("All users delete from MongoDB!"));
                resolve(1);
            }
        })
    });
}

//
// On ajoute les utilisateurs à notre collection
//
const add_Usercollection = (userArray) => {
    return new Promise( (resolve,reject) => {
        async.eachSeries(userArray, function iterator(item,callback) {
            item.save( (err) => {
                if( err ) {
                    console.error( err );
                }
                else {
                    console.info("=> Create acccout for " +color.green(item.login)+ " success");
                    callback( );
                }
            });
        }, function done( ) {
            console.info( color.yellow("Add user function done!") );
            resolve( 1 );
        });
    });
}

//
// Function du script final!
//
const script = function( ) {
    return new Promise( ( resolve, reject ) => {
        remove_Allusers( ).then( ( res ) => {
            const userArray = [];
            for(collection in UserStore)
                userArray.push( new UserDB( UserStore[collection] ) );
            add_Usercollection(userArray).then( ( res ) => {
                resolve( 1 );
            });
        });
    });
}

//
// Connexion à la database MongoDB
//
mongoose.connect('mongodb://localhost/Ethernium');
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

// Quand la base est ouverte!
db.once('open', function( ) {

    // Exécution du main!
    script( ).then( function( res ) {
        console.info("");
        console.info( color.green("All mongoDB action are executed successfully!") );
        db.close( ); // Close database!
    });
});
