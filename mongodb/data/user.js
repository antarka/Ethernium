var mongoose = require('mongoose');

// define the schema for our user model
var schema = module.exports = mongoose.model('User',{
    login: String,
    email: String,
    password: String,
    facebook: {
        id: String,
        token: String
    },
    twitter: {
        id: String,
        token: String
    },
    avatar: String,
    level: Number,
    right: Number,
    cgv: Boolean,
    cgu: Boolean,
    active: Number,
    friends: {},
});
