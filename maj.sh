#!/bin/bash

self_path=$(readlink -f "$0")
self_directory=$(dirname "${self_path}")
cd "${self_directory}"
tag=$1

iquit() {
	local error=$1
	local exitcode=$2

	if [ "$error" -ne "0" ]; then
		exit $exitcode
	fi
}

bash daemon.sh stop; iquit $? 4
git submodule update --init
git pull --recurse-submodules
git checkout $tag

npm install; iquit $? 2
npm run build; iquit $? 3

#
# Attente du fichier de config .gitignorer
# pour pouvoir l'éditer tranquilement
# Et tester le lancement
#

bash daemon.sh start; iquit $? 4
chown -R antarka:antarka .
exit 0