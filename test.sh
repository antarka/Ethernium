#!/bin/bash

self_path=$(readlink -f "$0")
self_directory=$(dirname "${self_path}")
cd "${self_directory}"

iquit() {
	local error=$1
	local exitcode=$2

	if [ "$error" -ne "0" ]; then
		exit $exitcode
	fi
}

apt-get install build-essential; iquit $? 1

npm run preinstall; iquit $? 2
npm install; iquit $? 2
npm run build; iquit $? 3

#
# Attente du fichier de config .gitignorer
# pour pouvoir l'éditer tranquilement
# Et tester le lancement
#

exit 0