const router = module.exports = require('koa-router')();

const connectedUser = function *(next) {
    if(this.session.user) {
        yield next;
    }
    else {
        this.body = "Sorry you didn't have the right!";
    }
}

router.get('/admin',connectedUser, function *(next) {
    this.body = "Admin page!";
});

router.get('/member', function *(next) {
    this.body = "Member page!";
});

router.get('/main',connectedUser, function *(next) {
    this.body = "Main page!";
});

router.get('/games/:name', function *(next) {
    this.render("games/"+this.params.name);
});

router.get('/partials/:name', function *(next) {
    this.render("partials/"+this.params.name);
});

router.get('*', function *(next) {
    this.render("layout");
});
