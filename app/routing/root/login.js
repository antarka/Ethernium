// NodeJS Package!
const mongoose    = require('mongoose');
const crypto      = require('crypto');
const koaRouter   = require('koa-router');

// Cryptage SHA1 function
var sha1sum = ( input ) => {
    return crypto.createHash('sha1').update(JSON.stringify(input)).digest('hex');
}

// Database user.js
const UserDB    = require('../../../mongodb/data/user.js');

const authRouter = module.exports = new koaRouter({
    prefix : '/authentification'
});

authRouter.post('/default', function *(next) {
    const email     = this.request.body.formData.email;
    const password  = this.request.body.formData.password;

    if(email && password) {
        mongoose.connect('mongodb://localhost/Ethernium');
        yield UserDB.find( { email : email, password: password} , (err,user) => {
            if (err) return console.log(err);
            this.session.user = user[0].login;
            this.redirect('/main');
        } );
    }
    else {
        this.body = {
            statusCode: 0,
            headerMsg: "Login or password empty!"
        };
    }
});
