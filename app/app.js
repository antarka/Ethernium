// NodeJS Packages
const path = require('path');

// KOAJS Framework packages
const koa           = require('koa');
const helmet        = require('koa-helmet');
const router        = require('koa-router')();
const serve         = require('koa-static');
const redisStore    = require('koa-redis');
const sessions      = require('koa-generic-session');
const koaJade       = require('koa-jade');
const bodyParser    = require('koa-bodyparser');
const koaJson       = require('koa-json');

// Declare application and export it!
const app           = module.exports = koa();
const manifest      = require('../manifest.json');

// Configure KOA Middleware (in try/catch)
try {

    // Apply ect as engine and views as main views directory!
    const jade = new koaJade({
        viewPath: path.join(__dirname,'views'),
        noCache: true,
        pretty: false,
        compileDebug: false,
        app: app
    });

    // Session middleware implementation !
    app.keys = ['keys', 'keykeys'];
    app.use( sessions( {
        store: redisStore({
            host : manifest.redis.host,
            port : manifest.redis.port
        })
    } ) );

    app.use( bodyParser() );

    // Serve static files under "static" repository.
    app.use( serve( __dirname + "/static" ) );

    // Securise http header.
    app.use( helmet() );

    app.use( koaJson() );

}
catch(Error) {
    console.error(Error);
}
