const TwitterStrategy  = require('passport-twitter').Strategy;
const User       = require('../models/users');
const configAuth = require('../../configs/social.json').twitter;

module.exports = function(passport) {

    passport.use(new TwitterStrategy({
        consumerKey     : configAuth.key,
        consumerSecret  : configAuth.secret,
        callbackURL     : configAuth.callback
    },
    function(token, tokenSecret, profile, done) {
        process.nextTick(function() {

            User.findOne({ 'twitter.id' : profile.id }, function(err, user) {
                if (err) return done(err);

                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    var newUser                 = new User();

                    // set all of the user data that we need
                    newUser.twitter.id          = profile.id;
                    newUser.twitter.token       = token;
                    newUser.login               = profile.displayName;

                    newUser.save(function(err) {
                        if (err) throw err;
                        return done(null, newUser);
                    });
                }
            });

    });

    }));

};
