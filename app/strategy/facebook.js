// load all the things we need
let FacebookStrategy    = require('passport-facebook').Strategy;
let FacebookUser        = require('../models/users');
let FacebookConfig      = require('../../configs/social.json').facebook;

module.exports = function(passport) {

    passport.use(new FacebookStrategy({
        clientID        : FacebookConfig.key,
        clientSecret    : FacebookConfig.secret,
        callbackURL     : FacebookConfig.callback
    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {
        process.nextTick(function() {
            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                if (err) return done(err);
                if (user) {
                    return done(null, user);
                } else {
                    var newUser            = new FacebookUser();
                    newUser.facebook.id    = profile.id;
                    newUser.facebook.token = token;
                    newUser.login          = profile.name.givenName + ' ' + profile.name.familyName;

                    newUser.save(function(err) {
                        if (err) throw err;
                        return done(null, newUser);
                    });
                }
            });
        });
    }));

};
