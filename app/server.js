// Require nodeJS clustering package !
const cluster   = require('cluster');
const http      = require('http');
const numCPUs   = 1;
//const numCPUs   = require('os').cpus().length;
const fs        = require('fs');
const path      = require('path');
const redis     = require('socket.io-redis');

process.env.NODE_ENV = 'development';

// Master process
const manifest = require('../manifest.json');

if (cluster.isMaster) {
    var i = 0;
    for (; i < numCPUs; i++)
        cluster.fork();
    console.info("Cluster => All process forked!");

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
}
else {
    // On include le core de l'application.
    const app = require('./app.js');

    // On recherche les routes
    const Route_global = require('./routing/global.js');
    const Route_Authentification = require('./routing/root/login.js');

    // On implémente les routes au middleware de l'application.
    app.use( Route_global.routes() ).use( Route_global.allowedMethods() );
    app.use( Route_Authentification.routes() ).use( Route_Authentification.allowedMethods() );

    // On importe et attache les sockets à notre application !
    const IO_Tchat = require('./sockets/tchat.js');
    IO_Tchat.attach(app);

    // On intègre un support de redis aux connections sockets (pour le load-balancing).
    app.io.adapter( redis({
        host: manifest.redis.host,
        port: manifest.redis.port
    }) );

    app.server.listen( process.env.PORT || manifest.app.port , process.env.IP || manifest.app.ip );
}
