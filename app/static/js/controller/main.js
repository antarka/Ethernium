(function () {
    var APP = angular.module("Ethernium", [
        'ngAnimate',
        'ngSocket',
        'ngProgress',
        'angular-preload-image',
        'duScroll',
        'etherRouter',
        'etherForm'
    ]);

    APP.value('duScrollDuration', 1200);

    APP.controller("layoutController",function($scope,$timeout,$location,ngProgressFactory) {
        console.log("layoutController loaded!");
        $scope.appShow = true;
        /*$scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.start();
        $timeout(function(){
            $scope.progressbar.complete();
            $scope.appShow = true;
        }, 1000); */

        $scope.router = function(focusPage) {
            $location.path(focusPage,true);
        }
    });

    APP.controller("loginController",function($scope,$http) {
        $scope.formData = {};
        $scope.submit = function() {
            console.log($scope.formData);
            $http.post('/authentification/default', {
                formData: $scope.formData
            }).success(function (data, status, headers, config) {
                console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        }
    });

    APP.controller("rootController",function($scope) {
        $scope.popup = false;
        $scope.more = false;
        $scope.discoverGame = function(gameName) {
            $scope.popup = !$scope.popup;
            console.log($scope.popup);
        }

        $scope.closePopup = function() {
            $scope.popup = false;
        }

        $scope.showMore = function() {
            $scope.more = !$scope.more;
        }
    });

})();
