(function() {
    var router = angular.module('etherRouter',['ngRoute']);

    router.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };
    }]);

    router.config(function ($routeProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'partials/root'
        })
        .when('/login', {
            templateUrl: 'partials/login'
        })
        .when('/games/lol', {
            templateUrl : 'games/lol'
        })
        .otherwise({
            redirectTo: '/'
        });
    });

})();
