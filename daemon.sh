#!/bin/bash

#################################
# Author : Purexo <contact@purexo.eu>
# usage :
# ./script {start|stop|force-stop|restart|reload|force-reload|status}
# number c'est pour le loadbalancing on va avoir plusieurs fois
# le même programme de lancé, faut pouvoir fermer ce que l'on veux
#################################

self_path=$(readlink -f "$0")
self_directory=$(dirname "${self_path}")
cd "${self_directory}"

PATH='/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games'
SCRIPTNAME="$0"
DESC='script de gestion de Daemon pour Ethernium'
DAEMONNAME='dEthernium'
DAEMON="/usr/bin/node"
DAEMONUSER='antarka'
PIDFILE="$self_directory/dEthernium.pid"

USAGE() {
	echo "Usage: bash $SCRIPTNAME {start|stop|force-stop|restart|reload|force-reload|status}"
}

if [ $# -lt 1 ]; then
	USAGE
	exit 1
fi

. /lib/lsb/init-functions

GETPID () {
	cat "$PIDFILE"
}

do_start () {
	log_daemon_msg "Starting system $DAEMONNAME Daemon"
	start-stop-daemon --name $DAEMONNAME --chuid "$DAEMONUSER" \
	--background --start --exec "$DAEMON" \
	--pidfile "$PIDFILE" --make-pidfile -- "$self_directory/app/server.js" '--harmony'
	log_end_msg $?
}

do_stop () {
	log_daemon_msg "Stopping system $DAEMONNAME Daemon"
	start-stop-daemon --pidfile "$PIDFILE" \
	--stop --retry 10
	log_end_msg $?
}

case "$1" in
	start)
		do_start
	;;
	stop)
		do_stop
		rm $PIDFILE
	;;

	restart|reload|force-reload)
		do_stop
		do_start
	;;

	force-stop)
		do_stop
		killall -q GETPID || true
		sleep 2
		killall -q -9 GETPID || true
		rm $PIDFILE
	;;

	status)
		status_of_proc -p $PIDFILE $DAEMON $DAEMONNAME && exit 0 || exit $?
	;;
	*)
		USAGE
		exit 1
	;;
esac
exit 0
