const color     = require('chalk');
const Q         = require('q');
const appDir    = process.cwd();

const argAction = process.argv[2];
if(!argAction) {
    argAction = "build";
}

const buildAction = {
    prebuild : function() {
        var deffered = Q.defer();
        console.log(color.yellow("prebuild executed!"));
        deffered.resolve("ok");
        return deffered.promise;
    },
    build : function() {
        var deffered = Q.defer();
        var MongoClean_RC = require('./mongodb/clean.js');
        deffered.resolve("ok");
        return deffered.promise;
    },
    postbuild : function() {
        var deffered = Q.defer();
        console.log(color.yellow("post executed!"));
        deffered.resolve("ok");
        return deffered.promise;
    }
}

buildAction[argAction]().then( function(res) {
    console.log(color.green(argAction + " is now finish without errors!"));
    console.log("");
});
